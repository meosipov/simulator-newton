/*
 * Newton. The gravity simulator.
 * Copyright (C) 2017  Михаил Осипов <osmiev@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "openglhelper.hpp"

#include <sstream>
#include <stdexcept>

#ifndef NDEBUG
std::string glError(GLenum err)
{
    switch (err)
    {
    case GL_INVALID_ENUM:
        return "Ivalid enumerate";

    case GL_INVALID_VALUE:
        return "Invalid value";

    case GL_INVALID_OPERATION:
        return "Ivalid operation";

    case GL_STACK_OVERFLOW:
        return "Stack overflow";

    case GL_STACK_UNDERFLOW:
        return "Stack underflow";

    case GL_OUT_OF_MEMORY:
        return "Out of memory";

    case GL_NO_ERROR:
    default:
        break;
    }

    return "";
}
#endif

OpenGLHelper &OpenGLHelper::instance()
{
    static OpenGLHelper helper;
    return helper;
}

#ifndef NDEBUG
void OpenGLHelper::asrt(const std::string &f, int l)
{
    const auto err = glGetError();

    if (err != GL_NO_ERROR)
    {
        std::stringstream ss;
        ss << glError(err) << " at " << f << ":" << l;
        throw std::runtime_error(ss.str());
    }
}
#endif

OpenGLHelper::OpenGLHelper()
    : QOpenGLFunctions_3_0()
{

}

OpenGLHelper::~OpenGLHelper()
{

}
