#pragma once

#include <list>
#include <numeric>

/**
 * @brief class MovingAverage Calculates SMA
 */
template <class T>
class MovingAverage
{
public:
    explicit MovingAverage(const std::size_t _size = 5)
        : m_size(_size)
    {
    }

    operator T() const
    {
        return m_value;
    }

    MovingAverage<T>& operator =(const T& value)
    {
        m_queue.push_back(value);

        if (m_queue.size() > m_size)
            m_queue.pop_front();

        m_value = std::accumulate(m_queue.begin(), m_queue.end(), 0) / m_queue.size();

        return *this;
    }

    void reset()
    {
        m_value = {};
        m_queue.clear();
    }

private:
    T m_value = T();
    std::size_t m_size;
    std::list <T> m_queue;

}; /// class MovingAverage

