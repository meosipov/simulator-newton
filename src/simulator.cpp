/*
 * Newton. The gravity simulator.
 * Copyright (C) 2017  Михаил Осипов <osmiev@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "simulator.hpp"

#include <cassert>

#include "simulation.hpp"

Simulator::Simulator(Simulation *sim)
{
    if (sim)
        setSimulation(sim);
}

Simulator::~Simulator()
{
    if (m_simulation)
        assert(false == m_simulation->isRunning() && "Simulation is still running");
}

bool Simulator::isRunning() const
{
    if (m_simulation)
        return m_simulation->isRunning();

    return false;
}

bool Simulator::setSimulation(Simulation* sim)
{
    assert(sim);
    assert(false == isRunning());

    sim->setSimulator(this);
    m_simulation.reset(sim);

    return true;
}

Simulation *Simulator::simulation() const
{
    return m_simulation.get();
}

bool Simulator::start()
{
    if (nullptr == m_simulation.get())
        return false;

    m_thread = std::thread(&Simulation::run, m_simulation.get());

    sgnSimulationStarted();

    return true;
}

void Simulator::stop()
{
    if (m_simulation && m_simulation->isRunning())
    {
        m_simulation->stop();
        sgnSimulationStopped();
    }
}

void Simulator::join()
{
    if (m_thread.joinable())
        m_thread.join();
}
