/*
 * Newton. The gravity simulator.
 * Copyright (C) 2017  Михаил Осипов <osmiev@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#pragma once

#include <atomic>
#include <memory>
#include <vector>

#include "object.hpp"

typedef struct SimData
{
//     std::size_t iteration = 0;
    std::vector <Object> objects;

} SIM_DATA;

class Simulator;

/**
 * @brief The Simulation class Abstract simulation class
 */
class Simulation
{
    friend class Simulator;

public:
    Simulation();
    virtual ~Simulation();

    /**
     * @brief run Start simulation loop
     */
    void run();

    /**
     * @brief stop Stop simulation
     */
    void stop();

    /**
     * @brief isRunning Return running status
     * @return status
     */
    inline bool isRunning() const
    {
        return m_isRunning;
    }

    /**
     * @brief getData Return simulation data
     * @param objects List of objects
     * @param iteration Iteration number
     */
    void getData(std::vector <Object> &objects, std::size_t &iteration);

protected:
    /**
     * @brief loop Iteration logic
     */
    virtual void loop() = 0;

    /**
     * @brief prepare Pre calculation setup
     */
    virtual void prepare() {}

private:
    /**
     * @brief setSimulator Link simulator
     * @param simulator Simulator
     */
    void setSimulator(Simulator *simulator);

protected:
    std::size_t m_iteration = 0;
    std::vector <Object> m_objects;

private:
    volatile std::atomic <bool> m_isRunning;
    Simulator *m_simulator = nullptr;

}; /// class Simulation

