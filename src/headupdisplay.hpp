/*
 * Newton. The gravity simulator.
 * Copyright (C) 2017  Михаил Осипов <osmiev@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#pragma once

#include "ui_headupdisplay.h"
#include "abstractwidgetwrapper.hpp"

#include "movingaverage.hpp"

namespace Ui
{
class HeadUpDisplay;
}

/**
 * @brief The HeadUpDisplay class This class provide head up display
 * @see AbstractWidgetWrapper
 */
class HeadUpDisplay final : public AbstractWidgetWrapper
{
    Q_OBJECT

signals:
    /**
     * @brief sgnRequestNew Emits when new button clicked
     */
    void sgnRequestNew();

public:
    explicit HeadUpDisplay(QWidget *parent = nullptr);
    ~HeadUpDisplay();

public slots:
    /**
     * @brief starMassChanged Set value for star mass indicator
     * @param starMass Star mass
     */
    void starMassChanged(double starMass);

    /**
     * @brief ipsChanged Set value for IPS indicator
     * @param ips iteration speed
     */
    void ipsChanged(std::size_t ips);

    /**
     * @brief fpsChanged Set value for FPS indicator
     * @param fps frame rate
     */
    void fpsChanged(std::size_t fps);

    /**
     * @brief reset Reset head up display
     */
    void reset();

private slots:
    /**
     * @brief updateDisplay Update most fastest data on display
     */
    void updateDisplay();

private:
    Ui::HeadUpDisplay *ui;

    MovingAverage <std::size_t> m_ips;
    MovingAverage <uint16_t> m_fps;
    double m_mass;

}; /// class HeadUpDisplay
