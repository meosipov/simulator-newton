/*
 * Newton. The gravity simulator.
 * Copyright (C) 2017  Михаил Осипов <osmiev@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#pragma once

#include <memory>
#include <thread>

#include "signal.hpp"

class Simulation;

/**
 * @brief The Simulator class This class manage simulations
 */
class Simulator
{
public:
    explicit Simulator(Simulation *sim = nullptr);
    ~Simulator();

    /**
     * @brief sgnIpsChanged emits when iteration speed changes
     */
    Signal <void, std::size_t> sgnIpsChanged;

    /**
     * @brief sgnIterationProceeded emits when new iteration proceeded
     */
    Signal <void, std::size_t> sgnIterationProceeded;

    /**
     * @brief sgnSimulationStarted emits when simulation started
     */
    Signal <void> sgnSimulationStarted;

    /**
     * @brief sgnSimulationStopped emits when simulation stopped
     */
    Signal <void> sgnSimulationStopped;

    /**
     * @brief setSimulation Set new simulation to simulator
     * @param sim
     * @return status
     * @note simulator must be stopped before calling this method
     */
    bool setSimulation(Simulation *sim);

    /**
     * @brief simulation Return pointer to current simulation
     * @return pointer
     */
    Simulation *simulation() const;

    /**
     * @brief start Start new simulation
     * @return status
     */
    bool start();

    /**
     * @brief stop Stop simulation
     */
    void stop();

    /**
     * @brief join Whait for thread stop
     */
    void join();

    /**
     * @brief isRunning Return status of the simulation
     * @return running status
     */
    bool isRunning() const;

private:
    std::unique_ptr <Simulation> m_simulation;
    std::thread m_thread;

}; /// class Simulator
