/*
 * Newton. The gravity simulator.
 * Copyright (C) 2017  Михаил Осипов <osmiev@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "genform.hpp"
#include "ui_genform.h"

#include <cassert>

#include "simulator.hpp"
#include "newtonsimulation.hpp"

GenForm::GenForm(Simulator *simulator, QWidget *parent)
    : AbstractWidgetWrapper(parent),
      ui(new Ui::GenForm),
      m_simulator(simulator)
{
    assert(simulator);

    ui->setupUi(this);

    connect(ui->m_btnDiscard, &QPushButton::clicked,
            this, &GenForm::deleteLater);

    connect(ui->m_btnOk, &QPushButton::clicked,
            this, &GenForm::restart);
}

GenForm::~GenForm()
{
    delete ui;
}

void GenForm::restart()
{
    m_simulator->stop();
    m_simulator->join();

    auto simNewt = new NewtonSimulation();

    simNewt->setCount(ui->m_sbCount->value());
    simNewt->setTimeStep(ui->m_sbTimeStep->value());
    simNewt->setStarDensity(ui->m_sbStarDensity->value());

    m_simulator->setSimulation(simNewt);
    m_simulator->start();

    deleteLater();
}
