/*
 * Newton. The gravity simulator.
 * Copyright (C) 2017  Михаил Осипов <osmiev@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "simulation.hpp"

#include <chrono>
#include <cassert>

#include "simulator.hpp"
#include "movingaverage.hpp"

Simulation::Simulation()
    :
    m_isRunning(false)
{
}

Simulation::~Simulation()
{
    assert(false == m_isRunning && "Simulation is still active");
}

void Simulation::run()
{
    assert(false == m_isRunning && "Simulation is allready started");

    m_isRunning = true;
    MovingAverage <std::size_t> ips;
    m_iteration = 0;

    prepare();

    while (m_isRunning)
    {
        auto p_timePrev = std::chrono::system_clock::now();

        loop();

        auto p_timeCur = std::chrono::system_clock::now();
        auto count = std::chrono::duration_cast<std::chrono::microseconds>(p_timeCur - p_timePrev).count();
        ips = 1e6 / count;

        m_simulator->sgnIterationProceeded(++m_iteration);
        m_simulator->sgnIpsChanged(ips);
    }
}

void Simulation::stop()
{
    assert(m_isRunning);

    m_isRunning = false;
}

void Simulation::getData(std::vector<Object> &objects, std::size_t &iteration)
{
    objects = m_objects;
    iteration = m_iteration;
}

void Simulation::setSimulator(Simulator *simulator)
{
    assert(simulator);

    m_simulator = simulator;
}
