/*
 * Newton. The gravity simulator.
 * Copyright (C) 2017  Михаил Осипов <osmiev@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#pragma once

#include <vector>

#include "simulation.hpp"

struct Object;

/**
 * @brief The NewtonSimulation class Newton potential realization
 */
class NewtonSimulation final : public Simulation
{
public:
    NewtonSimulation();

    /**
     * @brief setCount Set initial star count
     * @param count Star count
     */
    void setCount(std::size_t count);

    /**
     * @brief setTimeStep Set iteration time step
     * @param timeStep time step
     */
    void setTimeStep(double timeStep);

    /**
     * @brief setStarDensity Set star density
     * @param starDensity Star density
     */
    void setStarDensity(double starDensity);

protected:
    void loop() override;
    void prepare() override;

private:
    /**
     * @brief getSum Get summary gravitation data for selected object
     * @param obj Object
     * @return Gravitation data
     */
    Vector <double> getSum(const Object &obj) const;

    /**
     * @brief collapseObjects Process object collapsing
     * @param a Resulting object
     * @param b Secondary object
     * @return
     */
    void collapseObjects(Object &a, Object &b);

    /**
     * @brief updateSpeed Updates objects speed
     */
    void updateSpeed();

    /**
     * @brief checkCollapse Checks for objects collapse
     */
    void checkCollapse();

    /**
     * @brief radius
     * @param obj
     * @return
     */
    double radius(const Object &obj) const;

private:
    std::size_t	m_count = 1000;

    long double	m_timeStep = 1e3;
    long double	m_starDensity = 1e5;

//    double m_minStarMass = 0.3;

    /// Statistics
    Vector <double> m_bigPos;

}; /// class NewtonSimulation
