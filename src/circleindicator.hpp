/*
 * Newton. The gravity simulator.
 * Copyright (C) 2017  Михаил Осипов <osmiev@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#pragma once

#include <QWidget>
#include <QPropertyAnimation>

class QPaintEvent;

/**
 * @brief The CircleIndicator class This class is used to work as circle indica-
 * tor or progress indicator
 */
class CircleIndicator final : public QWidget
{
    Q_OBJECT

    Q_PROPERTY(float value READ value WRITE updateValue)

public:
    typedef enum DisplayMode : uint16_t
    {
        DM_Absolute,
        DM_Percent

    } DISPLAY_MODE;

    explicit CircleIndicator(QWidget *parent = nullptr);

    /**
     * @brief value Return current value
     * @return value
     */
    inline uint16_t value() const
    {
        return m_value;
    }

    /**
     * @brief maxValue Current max value
     * @return max value
     */
    inline uint16_t maxValue() const
    {
        return m_maxvalue;
    }

public slots:
    /**
     * @brief setDisplayMode Set indicator display mode
     * @param _mode
     * @see DisplayMode
     */
    void setDisplayMode(DisplayMode _mode);

    /**
     * @brief setValue Set current value
     * @param _val
     */
    void setValue(uint16_t _val);

    /**
     * @brief setMaxValue Set max value
     * @param _max
     */
    void setMaxValue(uint16_t _max);

    /**
     * @brief reset Reset indicator
     */
    void reset();

private:
    /**
     * @brief updateValue Update indicator value
     * @note need for animation
     * @param _val
     */
    void updateValue(float _val);

protected:
    void paintEvent(QPaintEvent *event) override;

private:
    float m_value = 0;
    float m_maxvalue = 100;
    DisplayMode m_mode = DM_Percent;

    bool m_animate = true;
    QPropertyAnimation m_animation;

}; /// class CircleIndicator;
