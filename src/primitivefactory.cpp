/*
 * Newton. The gravity simulator.
 * Copyright (C) 2017  Михаил Осипов <osmiev@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "primitivefactory.hpp"

#include <cmath>

#include "sceneobject.hpp"

PrimitiveFactory::PrimitiveFactory()
{

}

PrimitiveFactory::~PrimitiveFactory()
{

}

SceneActor PrimitiveFactory::createSphere(double radius, std::size_t slices)
{
    assert(radius > 0);

    /// Angle step
    const auto phiStep = M_PI / slices;

    const std::size_t vCount = 2 + 2 * slices * (slices-1);
    const std::size_t tCount = 4 * slices + (slices * 4) * (slices - 2);

    std::vector <GLdouble> verticles(3 * vCount);
    std::vector <GLdouble> normals(3 * vCount);
    std::vector <GLuint> indicies(3 * tCount);

    auto *vertex = reinterpret_cast <Vector<GLdouble>*> (&verticles[0]);
    auto *normal = reinterpret_cast <Vector<GLdouble>*> (&normals[0]);
    (*vertex)[2] = -radius;
    (*normal)[2] = -radius;

    /// Verticles
    for (auto i = 1; i < slices; ++i)
    {
        const double theta = -0.5 * M_PI + i * phiStep;

        for (auto j = 0u; j < slices*2; ++j)
        {
            double phi = 0 + j * phiStep;

            ++vertex;
            ++normal;

            (*normal)[0] = cos(phi) * cos(theta);
            (*normal)[1] = sin(phi) * cos(theta);
            (*normal)[2] = sin(theta);

            *vertex = *normal * radius;
        }
    }

    ++vertex;
    (*vertex)[2] = radius;
    ++normal;
    (*normal)[2] = radius;

    /// Triangles
    auto *fIndex = reinterpret_cast <Vector<GLuint>*> (&indicies[0]);

    std::size_t i0, i1, i2, i3, i4;

    /// Poles
    for (auto i = 0u; i < 2 * slices; ++i)
    {
        i0 = 0;
        i1 = i+1;
        i2 = i+2;

        if (i == 2 * slices - 1)
            i2 = i0 + 1;

        (*fIndex)[0] = i0;
        (*fIndex)[1] = i1;
        (*fIndex)[2] = i2;

        ++fIndex;

        i0 = verticles.size()/3 - 1;
        i1 = i0 - i - 1;
        i2 = i0 - i - 2;

        if (i == 2 * slices - 1)
            i2 = i0 - 1;

        (*fIndex)[0] = i0;
        (*fIndex)[1] = i1;
        (*fIndex)[2] = i2;

        ++fIndex;
    }

    /// Body
    for (auto i = 0; i < slices - 2; ++i)
    {
        i0 = 1 + i * 2 * slices;

        for (auto j = 0; j < 2 * slices; ++j)
        {
            i1 = i0 + j;
            i2 = i1 + 2 * slices;
            i3 = i1 + 1;
            i4 = i2 + 1;

            if (j == 2 * slices -1)
            {
                i3 = i0;
                i4 = i1 + 1;
            }

            (*fIndex)[0] = i1;
            (*fIndex)[1] = i2;
            (*fIndex)[2] = i3;

            ++fIndex;

            (*fIndex)[0] = i2;
            (*fIndex)[1] = i4;
            (*fIndex)[2] = i3;

            ++fIndex;
        }
    }

    SceneActor sphere;
    sphere.setVertex(verticles);
    sphere.setIndex(indicies, GL_TRIANGLES);
    sphere.setNormals(normals);

    return sphere;
}

// SceneActor PrimitiveFactory::createCube(const double &width)
// {
//     SceneActor cube;
// 
//     std::vector <GLdouble> p_vertex =
//     {
//         -0.5f, -0.5f, -0.5f,
//         0.5f, -0.5f, -0.5f,
//         0.5f,  0.5f, -0.5f,
//         -0.5f,  0.5f, -0.5f,
// 
//         -0.5f, -0.5f,  0.5f,
//         0.5f, -0.5f,  0.5f,
//         0.5f,  0.5f,  0.5f,
//         -0.5f,  0.5f,  0.5f,
//     };
//     std::transform(p_vertex.begin(), p_vertex.end(), p_vertex.begin(),
//                    [&](const GLdouble &v)
//     {
//         return v * width;
//     });
// 
//     const std::vector <GLuint> p_index =
//     {
//         0, 1, 1, 2, 2, 3, 3, 0,
//         0, 4, 1, 5, 2, 6, 3, 7,
//         4, 5, 5, 6, 6, 7, 7, 4,
//     };
// 
//     cube.setVertex(p_vertex);
//     cube.setIndex(p_index, GL_LINES);
// 
//     return cube;
// }
