#pragma once

#include <type_traits>
#include <functional>
#include <list>

/**
 * @brief the Signal class Custom signal implementation
 */
template <typename R, typename ... Args>
class Signal
{
public:
    Signal()
    {}

    ~Signal()
    {}

    template <class O>
    inline void connect ( O *object, R ( O::*member ) ( Args ... ) )
    {
        auto _callback = [=] ( Args ... args )
        {
            return ( object->*member ) ( std::forward<Args> ( args )... );
        };
        m_subscribes.push_back ( _callback );
    }

    template <class O>
    inline void disconnect ( O *object, R ( O::*member ) ( Args ... ) )
    {
        auto _callback = [=] ( Args ... args )
        {
            return ( object->*member ) ( std::forward<Args> ( args )... );
        };
        m_subscribes.erase ( _callback );
    }

    inline void connect ( std::function<R ( Args ... args ) > _function )
    {
        m_subscribes.push_back ( _function );
    }

//     inline void disconnect ( std::function<R ( Args ... args ) > _function )
//     {
//         m_subscribes.erase ( _function );
//     }

    inline void clear()
    {
        m_subscribes.clear();
    }

    inline void operator () ( Args ... args )
    {
        for ( auto callback : m_subscribes )
            callback ( std::forward <Args> ( args )... );
    }

private:
    std::list < std::function < R ( Args... ) > > m_subscribes;

}; /// class Signal

