#pragma once

#include "vector.hpp"

typedef struct Object
{
    std::size_t id = 0;

    double mass = .0;
    double radius = .0;

    Vector <double> pos;
    Vector <double> speed;

    /**
     * @brief isValid Check for valid mass
     * @note if you want to check positions, tou should check them separately
     * @return
     */
    bool isValid() const
    {
        return mass > 0.0;
    }

} OBJECT; /// struct Object
