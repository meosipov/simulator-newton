/*
 * Newton. The gravity simulator.
 * Copyright (C) 2017  Михаил Осипов <osmiev@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#pragma once

#include <QSet>
#include <QUuid>
#include <QHash>
#include <QOpenGLFunctions_3_0>

#include "vector.hpp"
#include "sceneobject.hpp"

typedef QHash <QUuid, SceneActor> SceneHash;

/**
 * @brief The Scene class This class is used to store scene actors
 */
class Scene final : protected QOpenGLFunctions_3_0
{
public:
    Scene();
    ~Scene();

    /**
     * @brief clearScene Clear all actors from scene
     */
    void clearScene();

    /**
     * @brief add Adds actor to scene
     * @param actor
     */
    void add(const SceneActor &actor);

    /**
     * @brief remove Removes actor from scene
     * @param uuid
     */
    bool remove(const QUuid &uuid);

    /**
     * @brief actor Return const reference to object
     * @param uuid
     * @return Const reference to scene actor
     */
    const SceneActor &actor(const QUuid &uuid) const;

    /**
     * @brief operator [] Return reference to actor
     * @param uuid
     * @return Reference to scene actor
     */
    SceneActor &operator[](const QUuid &uuid);

    /**
     * @brief hasActor Checks if actor exists
     * @param uuid
     * @return
     */
    bool hasActor(const QUuid &uuid) const;

    /**
     * @brief begin Return iterator to begin
     * @return
     */
    SceneHash::const_iterator begin() const;

    /**
     * @brief end Return iterator to end
     * @return
     */
    SceneHash::const_iterator end() const;

    /**
     * @brief flags Return scene flags
     * @return
     */
    QSet <GLenum> flags() const
    {
        return m_sceneFlags;
    }

    /**
     * @brief setFLag Sets scene flag
     * @param flag OpenGL flag
     * @param on flag mode
     */
    void setFLag(GLenum flag, bool on = true);

    /**
     * @brief clearFlags Clears scene flags
     */
    void clearFlags();

    /**
     * @brief lightPos Returns light position
     * @return
     */
    Vector <GLdouble> lightPos() const
    {
        return m_lightPos;
    }

    /**
     * @brief lightColor Returns light color
     * @return
     */
    Vector <GLfloat, 4> lightColor() const
    {
        return m_lightColor;
    }

    /**
     * @brief setLight Sets light
     * @param pos Light position
     * @param color Light color
     */
    void setLight(const Vector <GLdouble> &pos, const Vector <GLfloat, 4> &color);

    /**
     * @brief ambientColor return scene ambient color
     * @return
     */
    inline Vector <GLfloat, 4> ambientColor() const
    {
        return m_ambientColor;
    }

private:
    /// TODO: PIMPL

    /// Scene actors structure
    SceneHash m_sceneActors;

    QSet <GLenum> m_sceneFlags;

    Vector <GLdouble> m_lightPos;
    Vector <GLfloat, 4> m_lightColor;

    Vector <GLfloat, 4> m_ambientColor;

}; /// class Scene
