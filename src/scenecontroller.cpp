/*
 * Newton. The gravity simulator.
 * Copyright (C) 2017  Михаил Осипов <osmiev@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "scenecontroller.hpp"

#include <cassert>
#include <unordered_set>

#include <QTimer>

#include "scene.hpp"
#include "simulator.hpp"

#include "primitivefactory.hpp"

SceneController::SceneController(Scene *scene, Simulator *simulator,
                                 QObject *parent)
    : QObject(parent),
      m_scene(scene),
      m_simulator(simulator)
{
    assert(scene);
    assert(simulator);

    QTimer *timer = new QTimer(this);
    connect(timer, SIGNAL(timeout()), this, SLOT(updateScene()));
    timer->start(25);
}

SceneController::~SceneController()
{
}

void SceneController::updateIteration(std::size_t num)
{
    m_needUpdate = true;
}

void SceneController::updateScene()
{
    assert(m_scene);
    assert(m_simulator);

    /// TODO: Decompose

    if (false == m_needUpdate)
        return;

    m_scene->clearFlags();
    m_scene->setFLag(GL_BLEND);
    m_scene->setFLag(GL_DEPTH_TEST);

    std::vector <Object> objects;

    auto *sim = m_simulator->simulation();
    if (nullptr == sim)
        return;

    std::size_t iteration;
    sim->getData(objects, iteration);

    /// Saving id`s to set
    QSet <std::size_t> setObjects;
    setObjects.reserve(objects.size());
    std::for_each(objects.begin(), objects.end(), [&](const Object &obj)
    {
        setObjects |= obj.id;
    });

    reconfigure(objects);
    updateProjection(objects);

    /// First of all filter small objects to optimize calculation
    auto filterLess = [](std::vector <Object> &objList, double minMass)
    {
        auto iRem = std::remove_if(objList.begin(), objList.end(),
                                   [&](const Object &obj)
        {
            return obj.mass < minMass;
        });
        objList.erase(iRem, objList.end());
    };
    filterLess(objects, m_massiveMass);


    /// Search for star and set up light
    auto setLights = [this](std::vector <Object> &objList, double starMass)
    {
        auto iStar = std::find_if(objList.begin(), objList.end(),
                                  [&](const Object &obj)
        {
            return obj.mass >= starMass;
        });

        if (iStar != objList.end())
        {
            Vector <GLfloat, 4> color;
            color[0] = 2.0 - iStar->mass*2;
            color[1] = -16 * pow(iStar->mass + 0.25, 2) + 1;
            color[2] = iStar->mass*2 - 1;
            color[3] = 1.0;

            m_scene->setLight(iStar->pos, color);

            m_scene->setFLag(GL_LIGHTING);
            m_scene->setFLag(GL_LIGHT0);
        }
    };
    setLights(objects, m_starMass);

    /// Get list of unchanged objects to perform movement instead new object
    /// creation. Removing unchanged objects from original list.
    auto removeUnchanged = [this](std::vector <Object> &objList)
    {
        std::vector <Object> unchanged;
        unchanged.reserve(objList.size());

        auto rStart = std::remove_if(objList.begin(), objList.end(),
                                     [&, this](const Object &obj)
        {
            const auto iRecord = m_massiveObjects.find(obj.id);
            if (iRecord == m_massiveObjects.end())
                return false;

            if (obj.mass == iRecord->mass)
            {
                unchanged.push_back(obj);
                return true;
            }

            return false;
        });
        objList.erase(rStart, objList.end());

        return unchanged;
    };
    const auto unchangedList = removeUnchanged(objects);


    /// Move unchanged objects
    auto moveObjects = [this](const std::vector <Object> &movableList)
    {
        std::for_each(movableList.begin(), movableList.end(),
                      [this](const Object &obj)
        {
            const auto iRecord = m_massiveObjects.find(obj.id);
            assert(iRecord !=  m_massiveObjects.end());
            assert(m_scene->hasActor(iRecord->uuid));

            (*m_scene)[iRecord->uuid].setPos(obj.pos);
        });
    };
    moveObjects(unchangedList);

    /// Gathering object id`s to set for calcualtion optimization
    auto getSet = [](const std::vector <Object> &objList)
    {
        std::unordered_set <std::size_t> set;
        set.reserve(objList.size());

        std::for_each(objList.begin(), objList.end(), [&](const Object &obj)
        {
            set.insert(obj.id);
        });

        return set;
    };
    const auto setUnchanged = getSet(unchangedList);


    /// Clear objects in set from scene to perform new objects creation
    auto removeExcept = [this](const std::unordered_set <std::size_t> &setFilter)
    {
        const auto keys = m_massiveObjects.keys();

        std::for_each(keys.begin(), keys.end(), [&,this](const std::size_t id)
        {
            if (setFilter.count(id))
                return;

            const auto iRecord = m_massiveObjects.find(id);

            m_scene->remove(iRecord->uuid);
            m_massiveObjects.erase(iRecord);
        });
    };
    removeExcept(setUnchanged);


    /// Creates new objects
    auto createObjects = [this](const std::vector <Object> &objList)
    {
        GLfloat white[] = {1, 1, 1, 1};
        GLfloat black[] = {0, 0, 0, 1};

        PrimitiveFactory factory;

        std::for_each(objList.begin(), objList.end(),
                      [&,this](const Object &obj)
        {
            SceneActor massive = factory.createSphere(obj.radius, 10);
            massive.setPos(obj.pos);

            Material mat;
            mat.ambient = black;
            mat.diffuse = white;
            mat.shiness = 1;

            /// Star construction
            if (obj.mass >= m_starMass)
                mat.emission = m_scene->lightColor();

            massive.setMaterial(mat);

            m_scene->add(massive);

            m_massiveObjects[obj.id] = { massive.uuid(), obj.mass };
        });
    };
    createObjects(objects);

    m_needUpdate = false;
}

void SceneController::updateProjection(const std::vector <Object> &data)
{
    assert(m_scene);

    if (data.empty())
    {
        m_scene->remove(m_uuidProjection);
        return;
    }

    if (false == m_scene->hasActor(m_uuidProjection))
    {
        SceneActor aProj;
        m_uuidProjection = aProj.uuid();

        /// Ignore light
        GLfloat whiteColor[] = {1, 1, 1, 1};
        GLfloat blackColor[] = {0, 0, 0, 1};

        Material mat;
        mat.diffuse = blackColor;
        mat.emission = whiteColor;
        aProj.setMaterial(mat);

        m_scene->add(aProj);
    }
    else
        (*m_scene)[m_uuidProjection].clean();

    SceneActor &actor = (*m_scene)[m_uuidProjection];

    std::vector <GLdouble> vertex(3 * data.size());
    std::vector <GLuint> index(data.size());

    std::size_t i = 0;
    std::for_each(data.begin(), data.end(), [&](const Object &obj)
    {
        index[i] = i;

        auto &curVertex = *reinterpret_cast <Vector <GLdouble> *> (&vertex[3*i]);
        curVertex = obj.pos;

        ++i;
    });

    actor.setVertex(vertex);
    actor.setIndex(index, GL_POINTS);
}

void SceneController::reconfigure(const std::vector <Object> &objects)
{
    m_massCenter = QVector3D();
    Vector <double> massCenter;

    Vector <GLdouble> starSuperPos;

    bool hasStar = false;
    double starSummary = 0;
    double summMass = 0;

    std::for_each(objects.begin(), objects.end(), [&,this](const Object &obj)
    {
        if (obj.mass >= m_starMass)
        {
            hasStar = true;
            starSummary += obj.mass;
            starSuperPos += obj.pos * obj.mass;

            summMass += obj.mass;
        }

        /// Center mass calculation
        massCenter += obj.pos * obj.mass;
    });

    if (hasStar)
    {
        massCenter = starSuperPos / starSummary;
        emit sgnStarMassChanged(summMass);
    }
    else
        emit sgnStarMassChanged(0);

    m_massCenter[0] = massCenter[0];
    m_massCenter[1] = massCenter[1];
    m_massCenter[2] = massCenter[2];

    emit sgnMassCenterChanged(m_massCenter);
}
