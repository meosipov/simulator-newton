/*
 * Newton. The gravity simulator.
 * Copyright (C) 2017  Михаил Осипов <osmiev@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#pragma once

#include <QOpenGLWidget>
#include <QVector3D>
#include <QUuid>
#include <QSet>

class Scene;

/**
 * @brief The DisplayWidget class OpenGl display widget
 */
class DisplayWidget final : public QOpenGLWidget
{
    Q_OBJECT

signals:
    /**
     * @brief sgnFpsChanged Emits when fps change
     * @param fps
     */
    void sgnFpsChanged(std::size_t fps);

public:
    explicit DisplayWidget(QWidget *parent = nullptr);
    ~DisplayWidget();

    /**
     * @brief setScene Set scene link
     * @param scene
     */
    void setScene(Scene *scene);

    /**
     * @brief setFocusPos Set camera focus position
     * @param pos
     */
    void setFocusPos(const QVector3D &pos);

protected:
    void paintGL() override;
    void resizeGL(int w, int h) override;
    void initializeGL() override;

    void mouseMoveEvent(QMouseEvent *event) override;
    void mousePressEvent(QMouseEvent *event) override;
    void wheelEvent(QWheelEvent *event) override;
    void resizeEvent(QResizeEvent *e) override;

private:
    /**
     * @brief drawScene Draws scene
     * @param scene
     */
    void drawScene(Scene *scene);

    /**
     * @brief qNormalizeAngle Set angle in range from 0 to 360
     * @param angle angle
     */
    static void qNormalizeAngle(int &angle);

    /**
     * @brief setXRotation Rotate camera for x axis
     * @param angle Rotation angle
     */
    void setXRotation(int angle);

    /**
     * @brief setYRotation Rotate camera for Y axis
     * @param angle Rotation angle
     */
    void setYRotation(int angle);

    /**
     * @brief setZRotation Rotate camera for Z axis
     * @param angle Rotation angle
     */
    void setZRotation(int angle);

    /**
     * @brief Set distance to camera from focus
     * @param angle Distance
     */
    void setDistance(GLdouble _distance);

    /**
     * @brief setupCamera Set up camera settings
     */
    void setupCamera();

    /**
     * @brief calcFps Calculates frame rate
     */
    void calcFps();

    /**
     * @brief setupLighting Set up scene lighting
     * @param scene
     */
    void setupLighting(Scene *scene);

private:
    Scene *m_scene = nullptr;

    QPoint m_mousePos;

    /// Data for camera
    QVector3D m_rot;
    QVector3D m_focusPos;
    double m_rotSpeed = 0.5;
    GLdouble m_distance = 2.0f;

    /// Data for render
    GLdouble m_fov = 40;
    GLdouble m_ratio = 0;
    GLdouble m_zNear = 0.1;

    uint16_t m_fps;
    uint16_t m_outFps = 60;

}; /// class DisplayWidget
