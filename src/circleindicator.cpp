/*
 * Newton. The gravity simulator.
 * Copyright (C) 2017  Михаил Осипов <osmiev@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "circleindicator.hpp"

#include <algorithm>
#include <cmath>

#include <QStyle>

#include <QPainterPath>
#include <QPaintEvent>
#include <QPainter>
#include <QRect>

constexpr float INTERIOR_SCALE = 0.85;

CircleIndicator::CircleIndicator(QWidget *parent)
    : QWidget(parent),
      m_animation(this, "value")
{
}

void CircleIndicator::setDisplayMode(CircleIndicator::DisplayMode _mode)
{
    m_mode = _mode;
}

void CircleIndicator::setValue(uint16_t _val)
{
    if (m_animate)
    {
        m_animation.stop();
        m_animation.setStartValue(m_value);
        m_animation.setEndValue(_val);
        m_animation.setDuration(500);
        m_animation.start();
    }
    else
        m_value = _val;
}

void CircleIndicator::setMaxValue(uint16_t _max)
{
    m_maxvalue = _max;
}

void CircleIndicator::reset()
{
    m_animation.stop();
    m_value = 0;
}

// cppcheck-suppress unusedFunction
void CircleIndicator::updateValue(float _val)
{
    m_value = _val;
    update();
}

// cppcheck-suppress unusedFunction
void CircleIndicator::paintEvent(QPaintEvent *event)
{
    QPainter p(this);
    p.setRenderHint(QPainter::Antialiasing);

    const auto center = event->rect().center();

    auto p_size = size();
    const auto width = std::min(p_size.width(), p_size.height());
    QRectF p_rectExterior(0, 0, width * 0.9, width * 0.9);
    p_rectExterior.moveCenter(center);

    QRectF p_rectInterior(0, 0, p_rectExterior.width() * INTERIOR_SCALE,
                          p_rectExterior.width() * INTERIOR_SCALE);
    p_rectInterior.moveCenter(center);

    const auto radius = p_rectInterior.width() * 0.5;

    QPainterPath p_ppExternal;
    p_ppExternal.addEllipse(p_rectExterior);

    QPainterPath p_ppInternal;
    p_ppInternal.addEllipse(p_rectInterior);

    p_ppExternal = p_ppExternal.subtracted(p_ppInternal);

    QString p_strValue;
    switch (m_mode)
    {
    case DM_Absolute:
        p_strValue = QString::number(static_cast <int> (m_value));
        break;

    case DM_Percent:
        p_strValue = QString::number(static_cast <int> (100 * m_value / m_maxvalue)) + "%";
        break;

    default:
        break;
    }
    QRectF p_rectText(0, 0, radius * INTERIOR_SCALE * 2, radius * INTERIOR_SCALE * 2);
    p_rectText.moveCenter(center);

    QFont p_fValue(QFont("Sans", 0.25 *  p_rectText.height()));
    p_fValue.setBold(true);

    QPainterPath p_ppFill;
    const float angle = 360.0 * m_value / m_maxvalue;
    p_ppFill.moveTo(center);
    p_ppFill.arcTo(p_rectExterior, 90, - angle);
    p_ppFill.lineTo(center);
    p_ppFill.closeSubpath();

    /// Draw background
    p.setPen(Qt::NoPen);
    p.setBrush(palette().window());
    p.drawEllipse(p_ppExternal.boundingRect());

    /// Draw external ring fill
    p.setPen(Qt::NoPen);
    p.setBrush(palette().color(QPalette::Text).darker(200));
    p.drawPath(p_ppExternal);

    /// Draw value ring
    p.setPen(Qt::NoPen);
    p.setBrush(palette().text());
    p.setClipPath(p_ppExternal);
    p.drawPath(p_ppFill);
    p.setClipping(false);

    /// Draw external ring border
    p.setPen(palette().color(QPalette::Shadow));
    p.setBrush(Qt::NoBrush);
    p.drawPath(p_ppExternal);

    /// Draw text
    p.setFont(p_fValue);
    p.setPen(palette().color(QPalette::Text));
    p.drawText(p_rectText, p_strValue, QTextOption(Qt::AlignCenter));
}
