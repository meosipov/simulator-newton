/*
 * Newton. The gravity simulator.
 * Copyright (C) 2017  Михаил Осипов <osmiev@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#pragma once

#include <QWidget>
#include <QPointer>

/**
 * @brief The AbstractWidgetWrapper class Simple wrapper class which can be used
 * for various widget wrappers such as ProgressWrapper, HudWrapper, etc.
 */
class AbstractWidgetWrapper : public QWidget
{
    Q_OBJECT

public:
    explicit AbstractWidgetWrapper(QWidget *widget);
    ~AbstractWidgetWrapper();

protected:
    bool eventFilter(QObject * watched, QEvent * event) override;

private:
    QPointer <QWidget> m_wrapped;

}; /// class AbstractWidgetWrapper
