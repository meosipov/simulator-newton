/*
 * Newton. The gravity simulator.
 * Copyright (C) 2017  Михаил Осипов <osmiev@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "newtonsimulation.hpp"

#include <algorithm>

#include "object.hpp"

constexpr long double GRAVITY = 6.67234e-11;        /// Phys. Rev. Lett. 105, 110801 (2010)
//constexpr long double BOLZMAN = 1.3806488e-23;      /// Wikipedia

NewtonSimulation::NewtonSimulation()
    : Simulation()
{
}

void NewtonSimulation::setCount(std::size_t count)
{
    m_count = count;
}

void NewtonSimulation::setTimeStep(double timeStep)
{
    m_timeStep = timeStep;
}

void NewtonSimulation::setStarDensity(double starDensity)
{
    m_starDensity = starDensity;
}

void NewtonSimulation::prepare()
{
    m_objects.resize(m_count);

    std::srand(time(nullptr));

    long double totalMass = 0;
    std::size_t idObj = 0;

    /// Generating startup data
    std::generate(m_objects.begin(), m_objects.end(), [&]()
    {
        Object obj;
        obj.id = ++idObj;

        /// Randomizes coordinates
        for (std::size_t i = 0; i < 3; ++i)
            obj.pos[i] = (1.0 * (rand())/RAND_MAX-0.5);

        obj.mass = 1.0;
        /// Strong randomize
        for (auto i = 0; i < 12; ++i)
            obj.mass *= 1.0 * rand() / RAND_MAX;

        totalMass += obj.mass;

        return obj;
    });

    /// Calculates relative mass
    std::for_each(m_objects.begin(), m_objects.end(), [&](Object &obj)
    {
        obj.mass /= totalMass;
        obj.radius = radius(obj);
    });
}

Vector <double> NewtonSimulation::getSum(const Object &obj) const
{
    assert(obj.isValid());

    Vector <double> sum;

    Vector <double> diff;
    double dist = .0;
    double mod = .0;

    /// Using std::for_each instead std::accumulate because accumulate makes co-
    /// pies of returned object.
    std::for_each(m_objects.begin(), m_objects.end(),
                  [&](const Object &b)
    {
        dist = obj.pos.distanceTo(b.pos);
        if (dist == 0)
            return;

        diff = obj.pos - b.pos;
        mod = b.mass / (dist * dist * dist);

        sum[0] += diff[0] * mod;
        sum[1] += diff[1] * mod;
        sum[2] += diff[2] * mod;

        assert(sum.isValid());
    });

    return sum;
}

void NewtonSimulation::collapseObjects(Object &a, Object &b)
{
    assert(a.isValid() && b.isValid());

    a.speed = (a.speed * a.mass + b.speed * b.mass) / (a.mass + b.mass);

    /// Small object dissapears. Marking object with biggest mass to a because b
    /// will be removed after
    if (a.mass < b.mass)
        a.pos = b.pos;

    a.mass += b.mass;
    a.radius = radius(a);

    /// Mark object mass to 0 to erase after iteration
    b.mass = 0;
}

void NewtonSimulation::updateSpeed()
{
    std::for_each(m_objects.begin(), m_objects.end(), [this](Object &obj)
    {
        obj.speed += getSum(obj) * GRAVITY;
        obj.pos -= obj.speed * m_timeStep;

        assert(obj.isValid());
    });
}

void NewtonSimulation::checkCollapse()
{
    /// TODO: Optimize with octree

    std::size_t i, j;
    for (i = 0u; i < m_objects.size(); ++i)
    {
        Object &a = m_objects.at(i);
        if (false == a.isValid())
            continue;

        for (j = 0u; j < m_objects.size(); ++j)
        {
            Object &b = m_objects.at(j);
            if (false == b.isValid())
                continue;

            if (i == j || a.pos.distanceTo(b.pos) > a.radius + b.radius)
                continue;

            collapseObjects(a, b);
        }
    }
}

double NewtonSimulation::radius(const Object &obj) const
{
    assert(obj.isValid());

    return pow(3 * obj.mass / (4 * M_PI * m_starDensity), 1.0/3.0);
}

void NewtonSimulation::loop()
{
    if (m_count <= 1)
    {
        stop();
        return;
    }

    updateSpeed();

    checkCollapse();

    /// Remove objects with 0 mass (after collision)
    auto iBegin = std::remove_if(m_objects.begin(), m_objects.end(),
                                 [](const Object &obj)
    {
        return !obj.isValid();
    });
    m_objects.erase(iBegin, m_objects.end());
}
