/*
 * Newton. The gravity simulator.
 * Copyright (C) 2017  Михаил Осипов <osmiev@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#pragma once

#include <QHash>
#include <QUuid>
#include <QObject>
#include <QVector3D>

#include "simulation.hpp"

class Scene;
class Simulator;

/**
 * @brief The SceneController class This class is used to listen simulator and
 * manage scene
 */
class SceneController final : public QObject
{
    Q_OBJECT

signals:
    /**
     * @brief sgnMassCenterChanged Emits when mass center changed
     * @param pos
     */
    void sgnMassCenterChanged(const QVector3D pos);

    /**
     * @brief sgnStarMassChanged Emits when star mass changed
     * @param mass
     */
    void sgnStarMassChanged(double mass);

public:
    SceneController(Scene *scene, Simulator *simulator,
                    QObject *parent = nullptr);
    ~SceneController();

    /**
     * @brief updateIteration Clean update flag
     */
    void updateIteration(std::size_t num);

private slots:
    /**
     * @brief updateScene Updates scene
     */
    void updateScene();

private:
    /**
     * @brief updateProjection
     */
    void updateProjection(const std::vector<Object> &data);

    /**
     * @brief reconfigure
     */
    void reconfigure(const std::vector<Object> &objects);

private:
    /// TODO: PIMPL

    bool m_needUpdate = true;

    Scene *m_scene = nullptr;
    Simulator *m_simulator = nullptr;

    double m_starMass = 0.3;
    double m_massiveMass = 0.001;

    QVector3D m_massCenter;

    QUuid m_uuidProjection;

    typedef struct MassiveSubData
    {
        QUuid uuid;
        // cppcheck-suppress unusedStructMember
        double mass = 0.0;

    } MASSIVE_SUB_DATA;
    QHash <std::size_t, MassiveSubData> m_massiveObjects;

}; /// class SceneController
