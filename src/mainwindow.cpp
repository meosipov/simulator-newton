/*
 * Newton. The gravity simulator.
 * Copyright (C) 2017  Михаил Осипов <osmiev@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "mainwindow.hpp"

#include "scene.hpp"
#include "scenecontroller.hpp"

#include "simulator.hpp"
#include "displaywidget.hpp"
#include "newtonsimulation.hpp"

#include "genform.hpp"
#include "headupdisplay.hpp"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent),
      m_simulator(new Simulator),
      m_scene(new Scene),
      m_controller(new SceneController(m_scene.data(), m_simulator.data()))
{
    auto simNewt = new NewtonSimulation();
    m_simulator->setSimulation(simNewt);

    auto display = new DisplayWidget(this);
    display->setScene(m_scene.data());
    setCentralWidget(display);

    auto hud = new HeadUpDisplay(display);
    connect(hud, &HeadUpDisplay::sgnRequestNew,
            this, &MainWindow::createGenForm);

    connect(display, &DisplayWidget::sgnFpsChanged,
            hud, &HeadUpDisplay::fpsChanged);

    connect(m_controller.data(), &SceneController::sgnStarMassChanged,
            hud, &HeadUpDisplay::starMassChanged);

    connect(m_controller.data(), &SceneController::sgnMassCenterChanged,
            display, &DisplayWidget::setFocusPos);

    m_simulator->sgnIpsChanged.connect(hud, &HeadUpDisplay::ipsChanged);
    m_simulator->sgnIterationProceeded.connect(m_controller.data(), &SceneController::updateIteration);
    m_simulator->sgnSimulationStarted.connect(hud, &HeadUpDisplay::reset);

    m_simulator->start();
}

MainWindow::~MainWindow()
{
    m_simulator->stop();
    m_simulator->join();
}

void MainWindow::createGenForm()
{
    auto *genDisplay = new GenForm(m_simulator.data(), centralWidget());
    genDisplay->show();

    genDisplay->setGeometry(centralWidget()->geometry());
}
