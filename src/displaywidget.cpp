/*
 * Newton. The gravity simulator.
 * Copyright (C) 2017  Михаил Осипов <osmiev@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "displaywidget.hpp"

#include <cassert>

#include <QTimer>
#include <QDateTime>
#include <QMouseEvent>

#include "openglhelper.hpp"

#include "scene.hpp"

DisplayWidget::DisplayWidget(QWidget *parent)
    : QOpenGLWidget(parent)
{
    QTimer *timer = new QTimer(this);
    connect(timer, SIGNAL(timeout()), this, SLOT(repaint()));
    timer->start(1000/60-1);
}

DisplayWidget::~DisplayWidget()
{
}

void DisplayWidget::setScene(Scene *scene)
{
    assert(scene);
    m_scene = scene;
}

void DisplayWidget::setFocusPos(const QVector3D &pos)
{
    m_focusPos = pos;
}

// cppcheck-suppress unusedFunction
void DisplayWidget::paintGL()
{
    assert(m_scene);

    setupCamera();
    setupLighting(m_scene);
    drawScene(m_scene);

    calcFps();
}

// cppcheck-suppress unusedFunction
void DisplayWidget::resizeGL(int width, int height)
{
    auto &h = OpenGLHelper::instance();

    m_ratio = 1.0 * width / height;

    GLdouble fH = tan(m_fov / 360 * M_PI ) * m_zNear;
    GLdouble fW = fH * m_ratio;

    h.glMatrixMode(GL_PROJECTION);
    h.glLoadIdentity();

    h.glFrustum(-fW, fW, -fH, fH, m_zNear, 10000);
}

// cppcheck-suppress unusedFunction
void DisplayWidget::mouseMoveEvent(QMouseEvent *event)
{
    int dx = event->x() - m_mousePos.x();
    int dy = event->y() - m_mousePos.y();

    if (event->buttons() & Qt::LeftButton)
    {
        setXRotation(m_rot.x() + m_rotSpeed * dy);
        setYRotation(m_rot.y() + m_rotSpeed * dx);
    }
    else if (event->buttons() & Qt::RightButton)
    {
        setXRotation(m_rot.x() + m_rotSpeed * dy);
        setZRotation(m_rot.z() + m_rotSpeed * dx);
    }

    m_mousePos = event->pos();
}

// cppcheck-suppress unusedFunction
void DisplayWidget::mousePressEvent(QMouseEvent *event)
{
    m_mousePos = event->pos();
}

// cppcheck-suppress unusedFunction
void DisplayWidget::wheelEvent(QWheelEvent *event)
{
    setDistance(event->angleDelta().y());
    repaint();
}

void DisplayWidget::resizeEvent(QResizeEvent *e)
{
    QOpenGLWidget::resizeEvent(e);

    m_ratio = 1.0 * e->size().width() / e->size().height();
}

void DisplayWidget::drawScene(Scene *scene)
{
    assert(scene);

    auto &h = OpenGLHelper::instance();

    const auto flags = scene->flags();

    h.glPushAttrib(GL_ALL_ATTRIB_BITS);

    for (auto f : flags)
        h.glEnable(f);

    std::for_each(scene->begin(), scene->end(), [&](const SceneActor &obj)
    {
        h.glPushMatrix();

        obj.draw();

        h.glPopMatrix();
    });

    h.glPopAttrib();
}

void DisplayWidget::qNormalizeAngle(int &angle)
{
    while (angle < 0)
        angle += 360 * 16;
    while (angle > 360 * 16)
        angle -= 360 * 16;
}

void DisplayWidget::setXRotation(int angle)
{
    qNormalizeAngle(angle);
    if (angle != m_rot.x())
        m_rot.setX(angle);
}

void DisplayWidget::setYRotation(int angle)
{
    qNormalizeAngle(angle);
    if (angle != m_rot.y())
        m_rot.setY(angle);
}

void DisplayWidget::setZRotation(int angle)
{
    qNormalizeAngle(angle);
    if (angle != m_rot.z())
        m_rot.setZ(angle);
}

void DisplayWidget::setDistance(GLdouble _distance)
{
    m_distance -= _distance / 1000.0f * m_distance;
}

void DisplayWidget::setupCamera()
{
    auto &h = OpenGLHelper::instance();

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

    /// Camera rotation
    h.glTranslated(0, 0, -m_distance);
    h.glRotated(m_rot.x(), 1, 0, 0);
    h.glRotated(m_rot.y(), 0, 1, 0);
    h.glRotated(m_rot.z(), 0, 0, 1);

    /// Translate to scene focus
    h.glTranslated(-m_focusPos[0], -m_focusPos[1], -m_focusPos[2]);
}

void DisplayWidget::setupLighting(Scene *scene)
{
    auto &h = OpenGLHelper::instance();

    h.glPushMatrix();

    const auto lightPos = scene->lightPos();
    h.glTranslated(lightPos[0], lightPos[1], lightPos[2]);

    h.glLightfv(GL_LIGHT0, GL_DIFFUSE, scene->lightColor());

    h.glLightf(GL_LIGHT0, GL_CONSTANT_ATTENUATION, 1);
    h.glLightf(GL_LIGHT0, GL_LINEAR_ATTENUATION, 0);
    h.glLightf(GL_LIGHT0, GL_QUADRATIC_ATTENUATION, 0);

    const GLfloat zeroPos[] = {0, 0, 0, 1};
    h.glLightfv(GL_LIGHT0, GL_POSITION, zeroPos);

    h.glPopMatrix();
}

void DisplayWidget::calcFps()
{
    static QDateTime lastTime;

    const QDateTime currTime = QDateTime::currentDateTime();
    const auto elapsed = lastTime.msecsTo(currTime);

    if (elapsed)
    {
        m_fps = 1000 / elapsed;
        sgnFpsChanged(m_fps);
    }

    lastTime = currTime;
}

// cppcheck-suppress unusedFunction
void DisplayWidget::initializeGL()
{
    assert(m_scene);

    auto &h = OpenGLHelper::instance();
    h.initializeOpenGLFunctions();

    const auto color = m_scene->ambientColor();
    glClearColor(color[0], color[1], color[2], color[3]);

    h.glLightModeli(GL_LIGHT_MODEL_TWO_SIDE, GL_FALSE);
    h.glShadeModel(GL_SMOOTH);

    h.glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
}

