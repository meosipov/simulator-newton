/*
 * Newton. The gravity simulator.
 * Copyright (C) 2017  Михаил Осипов <osmiev@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sceneobject.hpp"

#include "openglhelper.hpp"

SceneActor::SceneActor()
    :
    m_uuid(QUuid::createUuid())
{
}

SceneActor::~SceneActor()
{
}

void SceneActor::setMaterial(const Material &material)
{
    m_material = material;
}

void SceneActor::setVertex(const std::vector<GLdouble> &array)
{
    auto &h = OpenGLHelper::instance();

    if (m_vbo)
        h.glDeleteBuffers(1, &m_vbo);

    h.glGenBuffers(1, &m_vbo);
    h.glBindBuffer(GL_ARRAY_BUFFER, m_vbo);
    h.glBufferData(GL_ARRAY_BUFFER, array.size() * sizeof(GLdouble),
                   array.data(), GL_DYNAMIC_DRAW);
    h.glBindBuffer(GL_ARRAY_BUFFER, 0);

    GL_ASSERT;
}

void SceneActor::setIndex(const std::vector<GLuint> &array, GLsizei mode)
{
    auto &h = OpenGLHelper::instance();

    if (m_ibo)
        h.glDeleteBuffers(1, &m_ibo);

    m_indexCount = array.size();
    m_drawMode = mode;

    h.glGenBuffers(1, &m_ibo);
    h.glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_ibo);
    h.glBufferData(GL_ELEMENT_ARRAY_BUFFER, array.size() * sizeof(GLuint),
                   array.data(), GL_DYNAMIC_DRAW);
    h.glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

    GL_ASSERT;
}

void SceneActor::setNormals(const std::vector<GLdouble> &array)
{
    auto &h = OpenGLHelper::instance();

    if (m_nbo)
        h.glDeleteBuffers(1, &m_nbo);

    h.glGenBuffers(1, &m_nbo);
    h.glBindBuffer(GL_ARRAY_BUFFER, m_nbo);
    h.glBufferData(GL_ARRAY_BUFFER, array.size() * sizeof(GLdouble),
                   array.data(), GL_DYNAMIC_DRAW);
    h.glBindBuffer(GL_ARRAY_BUFFER, 0);

    GL_ASSERT;
}

void SceneActor::clean()
{
    auto &h = OpenGLHelper::instance();

    if (m_vbo)
        h.glDeleteBuffers(1, &m_vbo);

    if (m_ibo)
        h.glDeleteBuffers(1, &m_ibo);

    if (m_nbo)
        h.glDeleteBuffers(1, &m_nbo);
}

void SceneActor::setPos(const Vector<GLdouble> &pos)
{
    m_pos = pos;
}

Vector<GLdouble> SceneActor::pos() const
{
    return m_pos;
}

GLdouble SceneActor::x() const
{
    return m_pos[0];
}

GLdouble SceneActor::y() const
{
    return m_pos[1];
}

GLdouble SceneActor::z() const
{
    return m_pos[2];
}

void SceneActor::draw() const
{
    assert(m_vbo);

    auto &h = OpenGLHelper::instance();

    h.glTranslated(m_pos[0], m_pos[1], m_pos[2]);
    GL_ASSERT;

    h.glEnableClientState(GL_VERTEX_ARRAY);
    GL_ASSERT;
    h.glBindBuffer(GL_ARRAY_BUFFER, m_vbo);
    GL_ASSERT;
    h.glVertexPointer(m_coords, vertexType(), 0, nullptr);
    GL_ASSERT;

    h.glEnableClientState(GL_INDEX_ARRAY);
    GL_ASSERT;
    h.glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_ibo);
    GL_ASSERT;
    h.glIndexPointer(indexType(), 0, nullptr);
    GL_ASSERT;

    if (m_nbo)
    {
        h.glEnableClientState(GL_NORMAL_ARRAY);
        GL_ASSERT;
        h.glBindBuffer(GL_ARRAY_BUFFER, m_nbo);
        GL_ASSERT;
        h.glNormalPointer(normalType(), 0, nullptr);
        GL_ASSERT;
    }

    h.glMaterialfv(m_material.face, GL_AMBIENT, m_material.ambient);
    GL_ASSERT;
    h.glMaterialfv(m_material.face, GL_DIFFUSE, m_material.diffuse);
    GL_ASSERT;
    h.glMaterialfv(m_material.face, GL_SPECULAR, m_material.specular);
    GL_ASSERT;
    h.glMaterialfv(m_material.face, GL_EMISSION, m_material.emission);
    GL_ASSERT;
    h.glMaterialf(m_material.face, GL_SHININESS, m_material.shiness);
    GL_ASSERT;

    h.glDrawElements(m_drawMode, m_indexCount, GL_UNSIGNED_INT, nullptr);
    GL_ASSERT;

    h.glDisableClientState(GL_VERTEX_ARRAY);
    GL_ASSERT;
    h.glDisableClientState(GL_INDEX_ARRAY);
    GL_ASSERT;
    h.glDisableClientState(GL_NORMAL_ARRAY);
    GL_ASSERT;
}
