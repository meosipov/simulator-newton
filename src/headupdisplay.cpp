/*
 * Newton. The gravity simulator.
 * Copyright (C) 2017  Михаил Осипов <osmiev@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "headupdisplay.hpp"
#include "ui_headupdisplay.h"

#include <QTimer>

HeadUpDisplay::HeadUpDisplay(QWidget *parent)
    : AbstractWidgetWrapper(parent),
      ui(new Ui::HeadUpDisplay)
{
    ui->setupUi(this);

    reset();

    ui->m_indMass->setMaxValue(100);
    ui->m_indIps->setDisplayMode(CircleIndicator::DM_Absolute);
    ui->m_indFps->setDisplayMode(CircleIndicator::DM_Absolute);
    ui->m_indFps->setMaxValue(60);

    auto *heartBeat = new QTimer(this);
    connect(heartBeat, SIGNAL(timeout()), this, SLOT(updateDisplay()));
    heartBeat->start(1000);

    connect(ui->m_btnNew, &QPushButton::clicked,
            this, &HeadUpDisplay::sgnRequestNew);
}

HeadUpDisplay::~HeadUpDisplay()
{
    delete ui;
}

void HeadUpDisplay::ipsChanged(std::size_t ips)
{
    m_ips = ips;
}

void HeadUpDisplay::fpsChanged(std::size_t fps)
{
    m_fps = fps;
}

void HeadUpDisplay::reset()
{
    ui->m_indMass->reset();
    ui->m_indFps->reset();
    ui->m_indIps->reset();

    m_fps.reset();
    m_ips.reset();
}

void HeadUpDisplay::starMassChanged(double starMass)
{
    m_mass = starMass;
}

void HeadUpDisplay::updateDisplay()
{
    ui->m_indIps->setValue(m_ips);

    /// Save best result
    if (m_ips > ui->m_indIps->maxValue())
        ui->m_indIps->setMaxValue(m_ips);

    ui->m_indFps->setValue(m_fps);

    if (m_mass > 0.3)
        ui->m_indMass->setValue(m_mass * 100);

    repaint();
}


