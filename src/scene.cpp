/*
 * Newton. The gravity simulator.
 * Copyright (C) 2017  Михаил Осипов <osmiev@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "scene.hpp"

#include <cassert>

Scene::Scene()
    : QOpenGLFunctions_3_0()
{
    m_ambientColor[0] = 0;
    m_ambientColor[1] = 0;
    m_ambientColor[2] = 0;
    m_ambientColor[3] = 1;
}

Scene::~Scene()
{
    clearScene();
}

void Scene::clearScene()
{
    std::for_each(m_sceneActors.begin(), m_sceneActors.end(),
                  [this](SceneActor &actor)
    {
        actor.clean();
    });

    m_sceneActors.clear();
}

void Scene::add(const SceneActor &actor)
{
    assert(false == m_sceneActors.contains(actor.uuid()));

    m_sceneActors[actor.uuid()] = actor;
}

bool Scene::remove(const QUuid &uuid)
{
    auto iActor = m_sceneActors.find(uuid);
    if (iActor == m_sceneActors.end())
        return false;

    iActor->clean();
    m_sceneActors.erase(iActor);

    return true;
}

const SceneActor &Scene::actor(const QUuid &uuid) const
{
    auto iActor = m_sceneActors.find(uuid);

    if (iActor == m_sceneActors.end())
        throw std::runtime_error("Actor does not exists at scene!");

    return *iActor;
}

SceneActor &Scene::operator[](const QUuid &uuid)
{
    auto iActor = m_sceneActors.find(uuid);

    if (iActor == m_sceneActors.end())
        throw std::runtime_error("Actor does not exists at scene!");

    return *iActor;
}

bool Scene::hasActor(const QUuid &uuid) const
{
    return m_sceneActors.contains(uuid);
}

SceneHash::const_iterator Scene::begin() const
{
    return m_sceneActors.begin();
}

SceneHash::const_iterator Scene::end() const
{
    return m_sceneActors.end();
}

void Scene::setFLag(GLenum flag, bool on)
{
    if (on)
        m_sceneFlags.insert(flag);
    else
        m_sceneFlags.remove(flag);
}

void Scene::clearFlags()
{
    m_sceneFlags.clear();
}

void Scene::setLight(const Vector<GLdouble> &pos, const Vector<GLfloat, 4> &color)
{
    m_lightPos = pos;
    m_lightColor = color;
}

