/*
 * Newton. The gravity simulator.
 * Copyright (C) 2017  Михаил Осипов <osmiev@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <cassert>
#include "abstractwidgetwrapper.hpp"

#include <QEvent>
#include <QResizeEvent>

AbstractWidgetWrapper::AbstractWidgetWrapper(QWidget* widget)
    : QWidget(widget)
{
    assert(widget);

    widget->installEventFilter(this);
}

AbstractWidgetWrapper::~AbstractWidgetWrapper()
{
}

// cppcheck-suppress unusedFunction
bool AbstractWidgetWrapper::eventFilter(QObject *watched, QEvent *event)
{
    /// All events for wrapped widget should pass.
    watched->event(event);

    /// Ther wrapper interface do only resize job. Inherited class need to add
    /// custom logic.
    switch (event->type())
    {
    case QEvent::Resize:
    {
        QResizeEvent *re = dynamic_cast <QResizeEvent*> (event);
        resize(re->size());
    }
    break;

    default:
        break;
    }

    /// Return allways false for multiple wrappers work.
    return false;
}

