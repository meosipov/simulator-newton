/*
 * Newton. The gravity simulator.
 * Copyright (C) 2017  Михаил Осипов <osmiev@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#pragma once

#include <cstddef>

class SceneActor;

/**
 * @brief The PrimitiveFactory class Primitive factory is used to generate some
 * primitives
 */
class PrimitiveFactory
{
public:
    PrimitiveFactory();
    ~PrimitiveFactory();

    /**
     * @brief createSphere Create sphere
     * @param radius Sphere radius
     * @param slices Number of slices
     * @return
     */
    SceneActor createSphere(double radius, std::size_t slices);

    /**
     * @brief createCube Create skeleton cube
     * @param width Cube width
     * @return
     */
//     SceneActor createCube(const double &width);

}; /// class PrimitiveFactory
