
#pragma once

#include <algorithm>
#include <cassert>
#include <cstring>
#include <vector>
#include <cmath>

#define OPERATION(TYPE) \
    Vector <T, S> &operator TYPE##=(const T& value) \
    { \
        for (auto i = 0; i < S; ++i) \
            m_data[i] TYPE##= value; \
        return *this; \
    } \
    Vector <T, S> operator TYPE(const T& value) const \
    { \
        Vector <T> res(*this); \
        res TYPE ##= value; \
        return res; \
    } \
    Vector <T, S> operator TYPE##=(const Vector <T, S> &other) \
    { \
        for (auto i = 0; i < S; ++i) \
            m_data[i] TYPE##= other.m_data[i]; \
        return *this; \
    } \
    Vector <T, S> operator TYPE(const Vector <T, S> &other) const \
    { \
        Vector <T, S> res; \
        for (auto i = 0; i < S; ++i) \
            res[i] = m_data[i] TYPE other.m_data[i]; \
        return res; \
    }


/**
 * @brief the Vector class Geometry vector realization
 */
template <class T, std::size_t S = 3>
struct Vector
{
private:
    T m_data [S];

public:
    Vector()
    {
        std::fill(m_data, m_data + S, 0);
    }

    Vector(const Vector &other)
    {
        *this = other;
    }

    explicit Vector(T *source)
    {
        assert(source);
        *this = source;
    }

    Vector &operator =(const Vector &other)
    {
        std::copy(other.m_data, other.m_data + S, m_data);
        return *this;
    }

    bool operator ==(const Vector &other)
    {
        return 0 == std::memcmp(other.m_data, m_data, sizeof(T) * S);
    }


    Vector &operator =(T *source)
    {
        assert(source);
        std::copy(source, source + S, m_data);
        return *this;
    }

    template <class O>
    operator Vector <O, S> () const
    {
        Vector <O,  S> result;

        for (auto i = 0u; i < S; ++i)
            result[i] = static_cast <O> (m_data[i]);

        return result;
    }

    operator T*()
    {
        return &m_data[0];
    }

    operator const T*() const
    {
        return &m_data[0];
    }

    OPERATION(*)
    OPERATION(/)
    OPERATION(+)
    OPERATION(-)

    T& operator [](std::size_t index)
    {
        assert(index < S);
        return m_data[index];
    }

    T at(const std::size_t index) const
    {
        assert(index < S);
        return m_data[index];
    }

//     T length() const
//     {
//         T res = 0;
// 
//         for (auto i = 0u; i < S; ++i)
//             res += m_data[i] * m_data[i];
// 
//         return sqrt(res);
//     }

    T distanceTo(const Vector &other) const
    {
        T res = 0;

        for (auto i = 0u; i < S; ++i)
        {
            T val = other.m_data[i] - m_data[i];

            /// pow(val, 2)
            val *= val;

            res += val;
        }

        return sqrt(res);
    }

    bool isValid() const
    {
        /// Check for NaN
        for (auto i = 0u; i < S; ++i)
            if (m_data[i] != m_data[i])
                return false;

        return true;
    }

}; /// struct Vector
