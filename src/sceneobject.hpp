/*
 * Newton. The gravity simulator.
 * Copyright (C) 2017  Михаил Осипов <osmiev@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#pragma once

#include <QUuid>
#include <QOpenGLFunctions>

#include "vector.hpp"

typedef struct Material
{
    Vector <GLfloat, 4> ambient;
    Vector <GLfloat, 4> diffuse;
    Vector <GLfloat, 4> specular;
    Vector <GLfloat, 4> emission;
    GLfloat shiness = 0;

    GLenum face = GL_FRONT_AND_BACK;

} MATERIAL;

/**
 * @brief The SceneActor class Scene actor class
 */
class SceneActor final
{
public:
    SceneActor();
    ~SceneActor();

    void draw() const;

    /**
     * @brief uuid Actors uuid
     * @return
     */
    inline QUuid uuid() const
    {
        return m_uuid;
    }

    /**
     * @brief drawMode Returns draw mode
     * @see GL_TRIANGLES
     * @see GL_POINTS
     * @see GL_LINES
     * @return OpenGL draw mode
     */
//     inline GLsizei drawMode() const
//     {
//         return m_drawMode;
//     }

    /**
     * @brief vertexType Returns vertex type
     * @note only GL_DOUBLE supported
     * @return
     */
    inline GLenum vertexType() const
    {
        return GL_DOUBLE;
    }

    /**
     * @brief normalType Returns normal type
     * @note only GL_DOUBLE supported
     * @return
     */
    inline GLenum normalType() const
    {
        return GL_DOUBLE;
    }

    /**
     * @brief indexType Returns index type
     * @note only GL_UNSIGNED_BYTE supported
     * @return
     */
    inline GLenum indexType() const
    {
        return GL_UNSIGNED_BYTE;
    }

    /**
     * @brief material Returns actors material
     * @return
     */
    inline const Material &material() const
    {
        return m_material;
    }

    /**
     * @brief setMaterial Sets actors material
     * @param material
     */
    void setMaterial(const Material &material);

    /**
     * @brief setVertex Set vertex array
     * @param array Array buffer
     */
    void setVertex(const std::vector <GLdouble> &array);

    /**
     * @brief setIndex Set index array
     * @param array Array buffer
     * @param mode OpenGL Draw mode
     * @see drawMode
     */
    void setIndex(const std::vector<GLuint> &array, GLsizei mode);

    /**
     * @brief setNormals Set normal array
     * @param array Array buffer
     */
    void setNormals(const std::vector <GLdouble> &array);

    /**
     * @brief clean Cleans buffers
     */
    void clean();

    /**
     * @brief setPos Sets actors position
     * @param pos
     */
    void setPos(const Vector <GLdouble> &pos);

    /**
     * @brief pos Returns actors scene position
     * @return
     */
    Vector <GLdouble> pos() const;

    GLdouble x() const;
    GLdouble y() const;
    GLdouble z() const;

private:
    GLuint m_vbo = 0;
    GLuint m_ibo = 0;
    GLuint m_nbo = 0;

    GLsizei m_indexCount = 0;
    GLsizei m_drawMode = GL_POINTS;
    GLsizei m_coords = 3;

    Vector <GLdouble> m_pos;

    Material m_material;

    QUuid m_uuid;

}; /// class SceneActor
