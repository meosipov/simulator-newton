#include <cppunit/TestFixture.h>
#include <cppunit/extensions/HelperMacros.h>

#include <cassert>

#include "movingaverage.hpp"

class MovingAverageTest final : public CppUnit::TestFixture
{
    CPPUNIT_TEST_SUITE(MovingAverageTest);

    CPPUNIT_TEST(testAverate);

    CPPUNIT_TEST_SUITE_END();

public:
    void testAverate()
    {
        const int data[] = {4, 8, 15, 16, 23, 42};
        const std::size_t count = sizeof(data) / sizeof(int);

        MovingAverage <int> sma(4);
        for (auto i = 0; i < count; ++i)
            sma = data[i];

        auto average = [](const std::vector <int> &v, std::size_t size) -> int
        {
            if (v.empty())
                return 0;

            const std::size_t start = size > v.size() ? 0 : v.size() - size;
            const std::size_t end = v.size();

            const auto a = std::accumulate(v.data() + start, v.data() + end, 0);
            return a / std::min(size, v.size());
        };

        std::vector <int> avg;
        for (auto i = 0; i < count; ++i)
            avg.push_back(data[i]);

        CPPUNIT_ASSERT(sma == average(avg, 4));
    }

}; /// class MovingAverageTest

CPPUNIT_TEST_SUITE_REGISTRATION(MovingAverageTest);
