#include <cppunit/TestFixture.h>
#include <cppunit/extensions/HelperMacros.h>

#include <random>
#include <cstdint>

#include "vector.hpp"

class VectorTest final : public CppUnit::TestFixture
{
    CPPUNIT_TEST_SUITE(VectorTest);

    CPPUNIT_TEST(testConstruct);
    CPPUNIT_TEST(testCopyConstructPointer);
    CPPUNIT_TEST(testCopyConstruct);
    CPPUNIT_TEST(testCopyOtherType);

//     CPPUNIT_TEST(testLength);
    CPPUNIT_TEST(testDistanceTo);

    CPPUNIT_TEST(testEqual);

    CPPUNIT_TEST(testOperationRef);
    CPPUNIT_TEST(testOperationCopy);
    CPPUNIT_TEST(testOperationSingle);

    CPPUNIT_TEST_SUITE_END();

public:
    void setUp()
    {
        std::srand(time(nullptr));
    }

    void testConstruct()
    {
        constexpr std::size_t num = 100;

        Vector <uint16_t, 100> testVect;

        bool construct = true;
        for (std::size_t i = 0u; i < num; ++i)
        {
            if (testVect.at(i) == 0)
                continue;

            construct = false;
            break;
        }

        CPPUNIT_ASSERT(construct);
    }

    void testCopyConstructPointer()
    {
        int source[] = {rand(), rand(), rand()};

        Vector <int, 3> testVect(source);

        bool pointerCopyContruct = true;
        for (auto i = 0; i < 3; ++i)
        {
            if (testVect.at(i) == source[i])
                continue;

            pointerCopyContruct = false;
            break;
        }

        CPPUNIT_ASSERT(pointerCopyContruct);
    }

    void testCopyConstruct()
    {
        Vector <int, 3> source;
        source [0] = rand();
        source [1] = rand();
        source [2] = rand();

        Vector <int> testVect(source);

        bool copyConstruct = true;
        for (auto i = 0; i < 3; ++i)
        {
            if (testVect.at(i) == source.at(i))
                continue;

            copyConstruct = false;
            break;
        }

        CPPUNIT_ASSERT(copyConstruct);
    }

    void testCopyOtherType()
    {
        Vector <double, 3> source;
        source [0] = rand() * rand() / RAND_MAX;
        source [1] = rand() * rand() / RAND_MAX;
        source [2] = rand() * rand() / RAND_MAX;

        Vector <int> testVect;
        testVect = source;

        bool copyOtherType = true;
        for (auto i = 0; i < 3; ++i)
        {
            if (testVect.at(i) == source.at(i))
                continue;

            copyOtherType = false;
            break;
        }

        CPPUNIT_ASSERT(copyOtherType);
    }

//     void testLength()
//     {
//         Vector <uint16_t, 3> data;
//         data [0] = 100.0 * rand() / RAND_MAX;
//         data [1] = 100.0 * rand() / RAND_MAX;
//         data [2] = 100.0 * rand() / RAND_MAX;
// 
//         const uint16_t length = sqrt(
//                                     pow(data.at(0), 2) +
//                                     pow(data.at(1), 2) +
//                                     pow(data.at(2), 2)
//                                 );
// 
//         CPPUNIT_ASSERT(length == data.length());
//     }

    void testDistanceTo()
    {
        const Vector <uint16_t, 3> from;
        Vector <uint16_t, 3> to;
        to [0] = 100.0 * rand() / RAND_MAX;
        to [1] = 100.0 * rand() / RAND_MAX;
        to [2] = 100.0 * rand() / RAND_MAX;

        const uint16_t length = sqrt(
                                    pow(to.at(0), 2) +
                                    pow(to.at(1), 2) +
                                    pow(to.at(2), 2)
                                );

        CPPUNIT_ASSERT(length == from.distanceTo(to));
    }

    void testEqual()
    {
        Vector <uint16_t, 3> data;
        data [0] = 1000.0 * rand() / RAND_MAX;
        data [1] = 1000.0 * rand() / RAND_MAX;
        data [2] = 1000.0 * rand() / RAND_MAX;

        Vector <uint16_t, 3> other = data;

        CPPUNIT_ASSERT(data == other);
    }

    void testOperationRef()
    {
        Vector <uint16_t, 3> data;
        data [0] = 1000.0 * rand() / RAND_MAX;
        data [1] = 1000.0 * rand() / RAND_MAX;
        data [2] = 1000.0 * rand() / RAND_MAX;

        Vector <uint16_t, 3> other = data;

        data[0] += 100;
        data[1] += 100;
        data[2] += 100;

        other += 100;

        CPPUNIT_ASSERT(data == other);
    }

    void testOperationCopy()
    {
        Vector <uint16_t, 3> other;
        other [0] = 100;
        other [1] = 100;
        other [2] = 100;

        Vector <uint16_t, 3> data;

        CPPUNIT_ASSERT((data + other) == other);
    }

    void testOperationSingle()
    {
        Vector <uint16_t, 3> other;
        other += (uint16_t)100u;

        Vector <uint16_t, 3> data;
        CPPUNIT_ASSERT((data + other) == other);

        other /= 100;
        CPPUNIT_ASSERT((data + 1) == other);
    }

}; /// class VectorTest

CPPUNIT_TEST_SUITE_REGISTRATION(VectorTest);
